-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 18. Agustus 2017 jam 12:10
-- Versi Server: 5.5.8
-- Versi PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `myweb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cate_apple`
--

CREATE TABLE IF NOT EXISTS `cate_apple` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `cate_apple`
--

INSERT INTO `cate_apple` (`no`, `cate`) VALUES
(1, 'Laptop'),
(2, 'Smartphone'),
(3, 'Komputer'),
(4, 'Ipod');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_apple`
--

CREATE TABLE IF NOT EXISTS `table_apple` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  `nama` varchar(155) NOT NULL,
  `price` varchar(155) NOT NULL,
  `troughline` varchar(155) NOT NULL,
  `brand` varchar(155) NOT NULL,
  `ket` text NOT NULL,
  `file` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `table_apple`
--

INSERT INTO `table_apple` (`no`, `cate`, `nama`, `price`, `troughline`, `brand`, `ket`, `file`) VALUES
(1, 'Laptop', 'Macbook Air', '9000000', 'Rp.10.000.000', 'Apple', 'keren banget produknya', 'mac.png'),
(2, 'Smartphone', 'Iphone 6S', '3000000', 'Rp.5.000.000', 'Apple', 'oekfokefokeofkoefkoe', 'ipp.png'),
(3, 'Smartphone', 'Iphone 7 Plus', '8000000', 'Rp.10.000.000', 'Apple', 'keren banget', 'iph7.png'),
(4, 'Komputer', 'Macbook Air Pc', '9000000', 'Rp.12.000.000', 'Apple', 'adkowkdowkdow', 'macor.png'),
(5, 'Smartphone', 'Iphone 5C-Green 16 Gb', '2000000', 'Rp.2.500.000', 'Apple', 'keren banget iphone nya', 'ip5c.png'),
(6, 'Tablet', 'Ipad Wifi 32 Gb', '8000000', 'Rp.10.000.000', 'apple', 'keren ipad nya', 'ipad.png'),
(7, 'Smartphone', 'Iphone 5S', '3000000', 'Rp.3.500.000', 'Apple', 'kakakakakkaa', 'ip5s.png'),
(8, 'Smartphone', 'Iphone 7 Black Jet', '7000000', 'Rp.8.000.000', 'Apple', 'keren banget iphonenya', 'ip7b.png'),
(9, 'Ipod', 'Ipod Wireless', '4000000', 'Rp.5.000.000', 'Apple', 'kakakkakakaka', 'ipod.png'),
(10, 'Smartphone', 'Iphone 4S-16 Gb', '1500000', 'Rp.2.000.000', 'Apple', 'kakakakkakakaka', 'ip4.png'),
(11, 'Laptop', 'Macbook Pro', '9000000', 'Rp.10.000.000', 'Apple', 'mkdmkmdkwmdwkd', 'pro.png'),
(12, 'Smartphone', 'Iphone 5C-32 Gb', '2000000', 'Rp.2.500.000', 'Apple', 'lallalallaal', 'ip5cc.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_beli`
--

CREATE TABLE IF NOT EXISTS `table_beli` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(155) NOT NULL,
  `nama_barang` varchar(155) NOT NULL,
  `harga_barang` varchar(155) NOT NULL,
  `qty` varchar(155) NOT NULL,
  `tanggal` date NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(155) NOT NULL,
  `jumlah_harga` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `table_beli`
--

INSERT INTO `table_beli` (`no`, `nama`, `nama_barang`, `harga_barang`, `qty`, `tanggal`, `alamat`, `email`, `jumlah_harga`) VALUES
(1, 'jsdijw', 'fef', 'wdiuwwo', 'jijwwd', '2017-08-03', '', 'djodjow', 'jdhwdh'),
(2, '', '', '', '', '0000-00-00', '', '', '0'),
(3, '', '', '', '', '0000-00-00', '', '', '0'),
(4, '', '', '', '', '0000-00-00', '', '', '0'),
(5, '', '', '', '', '0000-00-00', '', '', '0'),
(6, '', '', '', '', '0000-00-00', '', '', '0'),
(7, '', '', '', '', '0000-00-00', '', '', '0'),
(8, '', 'Iphone 6S', '3000000 ', '', '0000-00-00', '', '', '0'),
(9, '', 'Acer Predator-64 Gb Ram', '9000000 ', '', '0000-00-00', '', '', '0'),
(10, '', 'Iphone 6S', '3000000 ', '', '0000-00-00', '', '', '0'),
(11, '', 'Iphone 6S', '3000000 ', '', '0000-00-00', '', '', '0'),
(12, '', 'Macbook Air-1 Tb Hdd', '16000000 ', '', '0000-00-00', '', '', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_best`
--

CREATE TABLE IF NOT EXISTS `table_best` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  `nama` varchar(155) NOT NULL,
  `price` varchar(155) NOT NULL,
  `troughline` varchar(155) NOT NULL,
  `brand` varchar(155) NOT NULL,
  `ket` text NOT NULL,
  `file` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data untuk tabel `table_best`
--

INSERT INTO `table_best` (`no`, `cate`, `nama`, `price`, `troughline`, `brand`, `ket`, `file`) VALUES
(1, 'Game', 'Xbox 360', '5000000', 'Rp.6.000.000', 'Microsoft', 'mantap bang', 'xbox.png'),
(2, 'Game', 'PSP 1 Tb', '3000000', 'Rp.4.000.000', 'Sony', 'cok banget dah', 'psp.png'),
(3, 'Game', 'JoyStick Cool', '1000000', 'Rp.1.500.000', 'Anonym', 'mantap', 'joy.png'),
(4, 'Music', 'Music Box Sony', '2000000', 'Rp.2.100.000', 'Sony', 'kok keren yaa', 'music.png'),
(5, 'Baju', 'Levis T-Shirt', '300000', 'Rp.200.000', 'Levis', 'keren banget cuy bajunya', 'bajuu.png'),
(6, 'Jam', 'Jam tangan Wanita-Ungu', '180000', 'Rp.200.000', 'G-Woman', 'Keren banget', 'jam2.png'),
(8, 'Celana', 'Celana Jeans', '100000', 'Rp.150.000', 'Jeans', 'celana nya bagus banget, bahnnya juga keren', 'je.png'),
(10, 'Tas', 'Tas Berkualitas', '500000', 'Rp.600.000', 'Berrah', 'keren bat dah kok cuy bro', 'ta.png'),
(11, 'Baju', 'Baju Dickies', '80000', 'Rp.100.000', 'Dickies', 'keren', 'baj.png'),
(12, 'Smartphone', 'Asus Zenfone Max- 2 Gb Ram', '3000000', 'Rp.2.000.000', 'Asus', 'kaean', 'zen.png'),
(13, 'Tablet', 'Asiafone Tablet-10" 2Gb Ram', '2000000', 'Rp.2.500.000', 'Asiafone', 'kekekekkee', 'tab.png'),
(17, 'Sepatu', 'Sepatu kulit ular', '1000000', 'Rp.2.000.000', 'Snakky', 'kakkakakakak', 'pat.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_cate`
--

CREATE TABLE IF NOT EXISTS `table_cate` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `table_cate`
--

INSERT INTO `table_cate` (`no`, `cate`) VALUES
(1, 'Game'),
(3, 'Smartphone'),
(4, 'Televisi'),
(5, 'Laptop'),
(6, 'Music');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_cate2`
--

CREATE TABLE IF NOT EXISTS `table_cate2` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `table_cate2`
--

INSERT INTO `table_cate2` (`no`, `cate`) VALUES
(1, 'Game'),
(2, 'Smartphone'),
(3, 'Televisi'),
(4, 'Laptop'),
(5, 'Kalung'),
(6, 'Sepatu'),
(7, 'Tas'),
(8, 'Baju'),
(9, 'Jam Tangan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_cate3`
--

CREATE TABLE IF NOT EXISTS `table_cate3` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `table_cate3`
--

INSERT INTO `table_cate3` (`no`, `cate`) VALUES
(1, 'Game'),
(2, 'Music'),
(3, 'Baju'),
(4, 'Jam Tangan'),
(5, 'Tablet'),
(6, 'Celana'),
(7, 'Sepatu'),
(8, 'Tas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_cate4`
--

CREATE TABLE IF NOT EXISTS `table_cate4` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `table_cate4`
--

INSERT INTO `table_cate4` (`no`, `cate`) VALUES
(1, 'Speaker'),
(2, 'Smartphone'),
(3, 'Laptop'),
(4, 'Televisi'),
(5, 'Oven'),
(6, 'Kamera');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_item`
--

CREATE TABLE IF NOT EXISTS `table_item` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  `nama` varchar(155) NOT NULL,
  `price` varchar(155) NOT NULL,
  `troughline` varchar(155) NOT NULL,
  `brand` varchar(155) NOT NULL,
  `ket` text NOT NULL,
  `file` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `table_item`
--

INSERT INTO `table_item` (`no`, `cate`, `nama`, `price`, `troughline`, `brand`, `ket`, `file`) VALUES
(1, 'Televisi', 'Tv Sony With Bravia Engine', '14000000', 'Rp.20.000.000,-', 'Sony', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'tvt.png'),
(2, 'Smartphone', 'Iphone 6S', '3000000', 'Rp.5.000.000,-', 'apple', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'ipp.png'),
(3, 'Game', 'Playstation 4-Flat-500 Gb Hdd', '4000000', 'Rp.5.000.000,-', 'Sony', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'ps4.png'),
(4, 'Laptop', 'Macbook Air-1 Tb Hdd', '16000000', 'Rp.18.000.000,-', 'Apple', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'mac.png'),
(5, 'Tas', 'Tas merah keren banget', '500000', 'Rp.600.000,-', 'Garnier', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'tas.png'),
(6, 'Jam Tangan', 'Jam tangan anak kecil', '80000', 'Rp.100.000,-', 'G-Shock', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'jam.png'),
(7, 'Sepatu', 'Sepatu Perempuan', '300000', 'Rp.400.000,-', 'Kasogi', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'sepatu.png'),
(8, 'Kalung', 'Kalung Berlian', '2000000', 'Rp.2.500.000,-', 'G-Max', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'kalung.png'),
(9, 'Tas', 'Tas Cream Cool', '1000000', 'Rp.1.500.000', 'Garnier', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'tas2.png'),
(10, 'Baju', 'Baju Sherek All Size', '200000', 'Rp.300.000', 'Peter Says Denim', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'baju.png'),
(11, 'Sepatu', 'Sepatu Seport Abidas', '500000', 'Rp.1.000.000', 'Abidas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'patu.png'),
(12, 'Baju', 'Baju LongSleeve Cool', '120000', 'Rp.140.000', 'Polo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc venenatis viverra dictum. Morbi sagittis finibus scelerisque. In vestibulum ipsum ac lobortis rhoncus. Nulla venenatis sit amet metus vel pulvinar. Duis vitae bibendum magna, vel accumsan orci. Fusce leo odio, convallis ut leo non, condimentum pharetra erat. Mauris nec lectus eget nunc cursus tristique et in lorem. Phasellus libero elit, convallis at laoreet ac, dapibus a urna. Praesent tincidunt massa nulla, a vulputate sapien vulputate sed. Donec euismod ipsum eu risus efficitur viverra.', 'ju.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_login`
--

CREATE TABLE IF NOT EXISTS `table_login` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(155) NOT NULL,
  `password` varchar(155) NOT NULL,
  `namkang` varchar(155) NOT NULL,
  `konfpass` varchar(155) NOT NULL,
  `email` varchar(155) NOT NULL,
  `ttl` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `table_login`
--

INSERT INTO `table_login` (`no`, `user`, `password`, `namkang`, `konfpass`, `email`, `ttl`) VALUES
(1, 'damar', 'keren', '', '', '', '0000-00-00'),
(2, 'keren', 'damar', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_nav`
--

CREATE TABLE IF NOT EXISTS `table_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  `link` varchar(155) NOT NULL,
  `isi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `table_nav`
--

INSERT INTO `table_nav` (`id`, `cate`, `link`, `isi`) VALUES
(1, 'Home', 'index.php', ''),
(2, 'About', 'about.php', ''),
(3, 'Product', 'product.php', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_promo`
--

CREATE TABLE IF NOT EXISTS `table_promo` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `cate` varchar(155) NOT NULL,
  `nama` varchar(155) NOT NULL,
  `price` varchar(155) NOT NULL,
  `troughline` varchar(155) NOT NULL,
  `brand` varchar(155) NOT NULL,
  `ket` text NOT NULL,
  `file` varchar(155) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data untuk tabel `table_promo`
--

INSERT INTO `table_promo` (`no`, `cate`, `nama`, `price`, `troughline`, `brand`, `ket`, `file`) VALUES
(1, 'Smartphone', 'Iphone 7 Plus', '8000000', 'Rp.10.000.000', 'Apple', 'keren', 'iph7.png'),
(8, 'Laptop', 'Acer Predator-64 Gb Ram', '9000000', 'Rp.12.000.000', 'Acer', 'cool banget dah', 'per.png'),
(9, 'Smartphone', 'Xperia Z5 Premium', '8000000', 'Rp.10.000.000', 'Sony', 'keren bat dah', 'z5.png'),
(10, 'Kamera', 'Nikon-D320', '4000000', 'Rp.5.500.000', 'Nikon', 'waw mantap', 'kamera.png'),
(11, 'Oven', 'Oven Regza', '5000000', 'Rp.7.000.000', 'Regza', 'waw keren', 'oven.png'),
(12, 'Televisi', 'Toshiba Rekon-32"', '4000000', 'Rp.5.000.000', 'Toshiba', 'wah keren banget', 'tv2.png'),
(13, 'Laptop', 'Acer Aspire - I3', '9000000', 'Rp.10.000.000', 'Acer', 'keren bat dah', 'komp.png'),
(14, 'Speaker', 'Speaker Sony', '1000000', 'Rp.2.000.000', 'Sony', 'keren banget', 'speaker.png'),
(15, 'Televisi', 'Coby 32"', '3000000', 'Rp.4.000.000', 'Coby', 'wah keren banget', 'preview.jpg'),
(16, 'Speaker', 'Sony Big Sound Speaker', '2000000', 'Rp.3.000.000', 'Sony', 'wah speaker yang indah sekali', 'sp.png'),
(17, 'Kamera', 'Nikon Coolpix', '2000000', 'Rp.3.000.000', 'Nikon', 'keren deh kamera nya', 'nikon.png'),
(18, 'Smartphone', 'Iphone 7 Plus Black', '8000000', 'Rp.9.000.000', 'Apple', 'keren banget hpnya', 'ip77.png');
